import * as protogullApi from "../../protogullApi";
import "./index.css";

const phonenum = document.querySelector("#phonenum") as HTMLInputElement;
const customer_name = document.querySelector("#customer_name") as HTMLInputElement;
const company = document.querySelector("#company") as HTMLInputElement;
const processing = document.querySelector("#processing") as HTMLInputElement;
const sendBtn = document.querySelector("#sendBtn") as HTMLInputElement;

let data = {
    phonenum: phonenum.value,
    customer_name: customer_name.value,
    company: company.value,
    processing: processing.checked
};

function onChange() {
    data = {
        phonenum: phonenum.value,
        customer_name: customer_name.value,
        company: company.value,
        processing: processing.checked
    };

    if (data.phonenum.length > 0 && data.processing) {
        sendBtn.removeAttribute("disabled");
    } else {
        sendBtn.setAttribute("disabled", "disabled");
    }
}

function onClick() {
    protogullApi.send(data);
}

phonenum.addEventListener("keyup", onChange);
customer_name.addEventListener("keyup", onChange);
company.addEventListener("keyup", onChange);
processing.addEventListener("change", onChange);
sendBtn.addEventListener("click", onClick);
