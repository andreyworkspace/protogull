import * as protogullApi from "../../protogullApi";
import {IMsg} from "../../interface/IMsg";
import "./index.css";

const consultantInput = document.querySelector("#consultant") as HTMLInputElement;
const infoInput = document.querySelector("#info") as HTMLInputElement;
const linksInput = document.querySelector("#links") as HTMLElement;
const markBtn1 = document.querySelector("#markBtn1") as HTMLInputElement;
const markBtn2 = document.querySelector("#markBtn2") as HTMLInputElement;
const markBtn3 = document.querySelector("#markBtn3") as HTMLInputElement;
const markBtn4 = document.querySelector("#markBtn4") as HTMLInputElement;
const markBtn5 = document.querySelector("#markBtn5") as HTMLInputElement;
const closeBtn = document.querySelector("#closeBtn") as HTMLInputElement;

protogullApi.addEventListener((msg: IMsg) => {
    if (msg.type === "msg") {
        const {consultant, info, links} = msg.data;
        consultantInput.value = consultant || "";
        infoInput.value = info || "";
        if (links) {
            renderLinks(links);
        }
    }
});

function renderLinks(links: {name: string, url: string}[]) {
    linksInput.innerHTML = links.map(({url, name}) => {
        if(url.length > 0){
            return `<p><a href="http://${url}">${name}</a></p>`;
        }
        else {
            return `<p>${name}</p>`;
        }
    }).join("");
}

function onMark(mark: number) {
    protogullApi.send({mark});
}

function onClick() {
    protogullApi.send({close: true});
}


markBtn1.addEventListener("click", () => onMark(1));
markBtn2.addEventListener("click", () => onMark(2));
markBtn3.addEventListener("click", () => onMark(3));
markBtn4.addEventListener("click", () => onMark(4));
markBtn5.addEventListener("click", () => onMark(5));
closeBtn.addEventListener("click", onClick);
