import * as protogullApi from "../../protogullApi";
import {IMsg} from "../../interface/IMsg";
import "./index.css";

const statusInput = document.querySelector("#status") as HTMLInputElement;
const infoInput = document.querySelector("#info") as HTMLInputElement;

protogullApi.addEventListener((msg: IMsg) => {
    if (msg.type === "msg") {
        const {status, info} = msg.data;
        statusInput.value = status || "";
        infoInput.value = info || "";
    }
});
