import * as protogullApi from "../../protogullApi";
import "./index.css";

const check_product = document.querySelector("#check_product") as HTMLInputElement;
const check_price = document.querySelector("#check_price") as HTMLInputElement;
const check_support = document.querySelector("#check_support") as HTMLInputElement;
const check_other = document.querySelector("#check_other") as HTMLInputElement;
const text_other = document.querySelector("#text_other") as HTMLInputElement;
const sendBtn = document.querySelector("#sendBtn") as HTMLInputElement;

let data = {
    check_product: check_product.checked,
    check_price: check_price.checked,
    check_support: check_support.checked,
    check_other: check_other.checked,
    text_other: text_other.value
};

function onChange() {
    data = {
        check_product: check_product.checked,
        check_price: check_price.checked,
        check_support: check_support.checked,
        check_other: check_other.checked,
        text_other: text_other.value
    };

    if (data.check_other) {
        text_other.removeAttribute("hidden");
    } else {
        text_other.setAttribute("hidden", "hidden");
    }
}

function onClick() {
    protogullApi.send(data);
}

check_product.addEventListener("change", onChange);
check_price.addEventListener("change", onChange);
check_support.addEventListener("change", onChange);
check_other.addEventListener("change", onChange);
text_other.addEventListener("keyup", onChange);
sendBtn.addEventListener("click", onClick);
