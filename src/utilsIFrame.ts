import {getCustumerId, protoGull} from './protogull';
import {sendMsg} from './utilWS';
import {IFrame} from './interface/IFrame';
import {IMsg} from './interface/IMsg';
import {generateGuid} from './utils';
import {IRMsgDataAppQuestion} from './interface/IRMsg';

export function listenFrameMessage(event: MessageEvent) {
    const app: IFrame | undefined = findAppByEvent(event);
    if (app) {
        try {
            const {data, type, response} = event.data;
            switch (type) {
                case 'msg':
                    sendMsg({
                        body: {
                            type,
                            data
                        },
                        code: app.code,
                        response_require: true
                    });
                    break;
                case 'event':
                    sendMsg({
                        body: {
                            type,
                            data
                        },
                        code: app.code,
                        response_require: false
                    });
                    break;
                case 'customerid':
                    if (app.target){
                        sendToApp(app.target, {
                            type: 'customerid',
                            data: {
                                customerid: getCustumerId()
                            },
                            response: false
                        })
                    }
                    break;
            }
        } catch (e) {
            console.error(`error parsing`);
        }
    }
}

export function initContainer(): HTMLElement {
    const div = document.createElement('div');
    div.innerHTML = templateContainer.trim();
    const container = div.firstChild as HTMLElement;
    const open = container.querySelector('.open') as HTMLElement;
    const cancel = container.querySelector('.cancel') as HTMLElement;
    const authBtn = container.querySelector('#authBtn') as HTMLElement;
    open.addEventListener('click', showFrame);
    cancel.addEventListener('click', minFrame);
    authBtn.addEventListener('click', clickAuthBtn);
    return document.body.appendChild(container);
}

export function encoding(htmlString: string) {
    return `data:text/html;charset=utf-8,${encodeURIComponent(htmlString)}`;
}

export function createIframeElement(srcString: string): HTMLIFrameElement {
    const ifrm: HTMLIFrameElement = document.createElement("iframe");
    ifrm.setAttribute("src", srcString);
    ifrm.style.width = "640px";
    ifrm.style.height = "480px";
    return ifrm;
}

export function showAuthForm() {
    const authForm = document.querySelector("#authForm") as HTMLElement;
    authForm.style.display = "block";
}

export function hideAuthForm() {
    const authForm = document.querySelector("#authForm") as HTMLElement;
    authForm.style.display = "none";
}

export function sendToApp(app: HTMLIFrameElement, msg: IMsg) {
    app.contentWindow && app.contentWindow.postMessage(msg, '*')
}

function btnOpenShow() {
    const open = document.querySelector('.open') as HTMLElement;
    open.style.display = "block";
}

function btnOpenHide() {
    const open = document.querySelector('.open') as HTMLElement;
    open.style.display = "none";
}

function showFrame() {
    const gullFrameWrapper = document.querySelector('#gullFrameWrapper') as HTMLElement;
    btnOpenHide();
    gullFrameWrapper.style.display = "block";
}

function minFrame() {
    const gullFrameWrapper = document.querySelector('#gullFrameWrapper') as HTMLElement;
    btnOpenShow();
    gullFrameWrapper.style.display = "none";
}

export function showQuestion(questionData: IRMsgDataAppQuestion) {
    const {timeout, variants, question} = questionData;
    const gullQuestion = document.querySelector('#gullQuestion') as HTMLElement;
    const gullQuestionText = document.querySelector('#gullQuestionText') as HTMLElement;
    const gullQuestionVariants = document.querySelector('#gullQuestionVariants') as HTMLElement;
    minFrame();
    btnOpenHide();
    gullQuestion.style.display = "block";
    gullQuestionText.innerHTML = question;
    gullQuestionVariants.innerHTML = '';
    const timeoutTarget = setTimeout(() => {
        answer('', true);
    }, timeout * 1000);
    Object.entries(variants).forEach(([variantCode, variantText]) => {
        const btnElement = document.createElement('button');
        const brElement = document.createElement('br');
        btnElement.innerHTML = variantText;
        btnElement.addEventListener('click', () => {
            answer(variantCode);
            clearTimeout(timeoutTarget);
        });
        gullQuestionVariants.appendChild(btnElement);
        gullQuestionVariants.appendChild(brElement);
    });
}

function answer(answer: string, timeout: boolean = false) {
    sendMsg({
        timeout,
        answer
    }, 'app_answer');
    minQuestion();
}

export function minQuestion() {
    const gullQuestion = document.querySelector('#gullQuestion') as HTMLElement;
    btnOpenShow();
    gullQuestion.style.display = "none";
}

function clickAuthBtn(e: MouseEvent) {
    e.preventDefault();
    e.stopPropagation();
    const target: HTMLElement = e.target as HTMLElement;
    const form: HTMLElement = target.parentElement as HTMLElement;
    const login: HTMLInputElement | null = form.querySelector('#login');
    const cookie = generateGuid();
    localStorage.setItem('protogullCookie', cookie);
    if (login) {
        sendMsg({
            'ext.name': login.value,
            cookie
        }, 'customerid');
    }
}

const templateContainer = `
    <div id="gullFrame">
        <div id="gullFrameWrapper">
            <div id="authForm">
                    <h1>Login</h1>
                    <label for="login"><b>login</b></label>
                    <input type="text" placeholder="login" id="login" required>
                    <button type="submit" class="btn" id="authBtn">login</button>
            </div>
            <div id="appFrame"></div>
            <button class="btn cancel">min</button>
        </div>
        <div id="gullQuestion">
            <p id="gullQuestionText"></p>
            <p id="gullQuestionVariants"></p>
        </div>
        <button class="btn open">open</button>
    </div>
`;

function findAppByEvent(event: MessageEvent): IFrame | undefined {
    return protoGull.apps.find(app => {
        return app.target && app.target.contentWindow === event.source;
    });
}
