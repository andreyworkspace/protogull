import {getCustumerId, getProtogullCookie, listenerWSMessage, protoGull} from './protogull';
import {generateGuid} from './utils';
import {TAction} from './interface/TAction';

export function createWSC(): WebSocket {
    return new WebSocket(protoGull.url);
}

export function initWSListeners() {
    protoGull.ws.addEventListener('open', ev => {
        const customerid = getCustumerId();
        if(customerid) {
            sendMsg({
                cookie: getProtogullCookie()
            }, 'customerid');
        }
    });

    protoGull.ws.addEventListener('close', ev => {
        if (ev.wasClean) {
            console.log('connect close correct');
        } else {
            console.log(`connect close correct. code: ${ev.code} reason: ${ev.reason}`);
            setTimeout(() => {
                protoGull.ws = createWSC();
                initWSListeners();
            }, 3000);
        }
    });

    protoGull.ws.addEventListener('message', ev => {
        try {
            const messageR: any = JSON.parse(ev.data);
            listenerWSMessage(messageR[1].data[0]);
        } catch (e) {
            console.log('error parsing');
        }
    });
}

export function sendMsg(data: object, action: TAction = 'message_request') {
    const msgTpl = ['message', {
        data: {
            action,
            qid: generateGuid(),
            customer_id: getCustumerId(),
            data
        }
    }];
    protoGull.ws.send(JSON.stringify(msgTpl));
}
