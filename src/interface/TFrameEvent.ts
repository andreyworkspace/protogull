export type TFrameEvent = 'loaded' | 'start' | 'stop' | 'wait';
