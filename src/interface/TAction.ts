export type TAction =
    'customerid'
    | 'message_request'
    | 'rapps_status'
    | 'app_request'
    | 'app_response'
    | 'rapps_upload'
    | 'rapps_delete'
    | 'rapps_start'
    | 'rapps_stop'
    | 'snapshot'
    | 'app_question'
    | 'app_answer'
    | 'message';
