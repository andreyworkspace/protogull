import {TMsq} from './TMsq';

export interface IMsg {
    type: TMsq;
    data: {
        [name: string]: any
    };
    response: boolean;
}
