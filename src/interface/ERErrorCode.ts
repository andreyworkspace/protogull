export enum ERErrorCode {
    NOERROR = 0,
    SYSTEMERROR = 100,
    REQUESTPARAMS = 110,
    AUTHERROR = 120,
    NOTFOUND = 210,
    NOTUNIQUE = 220
}
