import {TFrameState} from './TFrameState';

export interface IFrame {
    code: string;
    data: string;
    type: TFrame;
    state?: TFrameState;
    target?: HTMLIFrameElement;
}
