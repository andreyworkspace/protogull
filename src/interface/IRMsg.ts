import {TAction} from './TAction';
import {ERErrorCode} from './ERErrorCode';

export interface IRMsg {
    action: TAction,
    qid: string,
    data: TRMsgData
}

export type TRMsgData =
    IRMsgDataApp
    | IRMsgDataAppUpload
    | IRMsgDataCustomer
    | IRMsgDataAppRequest
    | IRMsgDataAppResponse
    | IRMsgDataAppQuestion
    | IRMsgDataSnapshot;

export interface IRMsgData {
    error_code: ERErrorCode;
    error: null | string;
}

export interface IRMsgDataApp extends IRMsgData {
    code: string,
}

export interface IRMsgDataAppUpload extends IRMsgDataApp {
    html: string
}

export interface IRMsgDataSnapshot extends IRMsgDataApp {
    select: string
}

export interface IRMsgDataCustomer extends IRMsgData {
    customer_id: string | null;
}

export interface IRMsgDataAppRequest extends IRMsgDataApp {
    body: object,
    response_require: boolean
}

export interface IRMsgDataAppResponse extends IRMsgData {
    body: object,
}

export interface IRMsgDataAppQuestion extends IRMsgData {
    question: string,
    variants: {
        [code: string]: string
    },
    timeout: number
}
