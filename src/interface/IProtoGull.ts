import {IFrame} from './IFrame';

export interface IProtoGull {
    ws: WebSocket;
    container: HTMLElement;
    url: string;
    apps: IFrame[];
}
