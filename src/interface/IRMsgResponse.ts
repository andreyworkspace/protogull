import {IRMsg} from './IRMsg';

export interface IRMsgResponse extends IRMsg{
    success: boolean,
}
