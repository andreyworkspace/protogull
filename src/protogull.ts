import {
    createIframeElement,
    encoding,
    hideAuthForm,
    initContainer,
    listenFrameMessage,
    sendToApp,
    showAuthForm, showQuestion
} from './utilsIFrame';
import {createWSC, initWSListeners, sendMsg} from './utilWS';
import './index.css';
import {
    IRMsg,
    IRMsgDataAppUpload,
    IRMsgDataCustomer,
    IRMsgDataSnapshot,
    IRMsgDataApp, IRMsgDataAppQuestion, IRMsgDataAppRequest
} from './interface/IRMsg';
import {IFrame} from './interface/IFrame';
import {IProtoGull} from './interface/IProtoGull';
import {ERErrorCode} from './interface/ERErrorCode';

const url: string = getUrl();
let protoGull: IProtoGull;

function init() {
    protoGull = {
        ...protoGull,
        url,
        apps: []
    };
    protoGull.ws = createWSC();
    protoGull.container = initContainer();
    initWSListeners();
    authCheck();
    window.addEventListener('message', listenFrameMessage, false);
}

document.addEventListener('DOMContentLoaded', () => init());

function authCheck() {
    const protogullCookie = getProtogullCookie();
    if (protogullCookie === null) {
        showAuthForm();
    } else {
        hideAuthForm();
    }
}

function getUrl(): string {
    const {currentScript} = document;
    const dataset: DOMStringMap | null = currentScript ? currentScript['dataset'] : null;
    const url: string | undefined = dataset ? dataset['url'] : undefined;
    if (dataset === null || url === undefined) {
        throw Error('dataset url !!!');
    }
    return url;
}

export function getCustumerId(): string | null {
    return localStorage.getItem('protogullCustomerid');
}

export function getProtogullCookie(): string | null {
    return localStorage.getItem('protogullCookie');
}

export function listenerWSMessage(message: IRMsg) {
    switch (message.action) {
        case 'customerid': {
            const {customer_id, error_code} = message.data as IRMsgDataCustomer;
            if (error_code === ERErrorCode.NOERROR && customer_id !== null) {
                localStorage.setItem('protogullCustomerid', customer_id);
                hideAuthForm();
            } else {
                localStorage.removeItem('protogullCustomerid');
                localStorage.removeItem('protogullCookie');
                showAuthForm();
            }
            break;
        }
        case 'message_request': {
            const {code, body, response_require} = message.data as IRMsgDataAppRequest;
            const app = findApp(code);
            if (app && app.target) {
                sendToApp(app.target, {
                    type: 'msg',
                    data: body,
                    response: response_require
                });
            }
            break;
        }
        case 'rapps_status': {
            sendMsg({
                rapps: [
                    ...protoGull.apps.map(app => {
                        return {
                            code: app.code,
                            status: app.state
                        }
                    }),
                    {
                        code: 'root',
                        status: 'root'
                    }
                ]
            }, 'rapps_status');
            break;
        }
        case 'rapps_upload': {
            const {code, html} = message.data as IRMsgDataAppUpload;
            if (findApp(code) === undefined) {
                protoGull.apps.push({
                    code,
                    data: html,
                    type: 'html',
                    state: 'waiting'
                });
            }
            break;
        }
        case 'rapps_delete': {
            const {code} = message.data as IRMsgDataApp;
            deleteApp(code);
            break;
        }
        case 'rapps_start': {
            const {code} = message.data as IRMsgDataAppUpload;
            const app: IFrame | undefined = findApp(code);
            if (app && app.data) {
                app.target = insertApp(createIframeElement(encoding(app.data)));
                app.state = 'active';
            }
            break;
        }
        case 'rapps_stop': {
            const {code} = message.data as IRMsgDataApp;
            deleteApp(code);
            break;
        }
        case 'snapshot':
            const {code, select} = message.data as IRMsgDataSnapshot;
            const app = findApp(code);
            if (app && app.target) {
                sendToApp(app.target, {
                    response: false,
                    type: 'snapshot',
                    data: {
                        select
                    }
                })
            }
            break;
        case 'app_question': {
            if (message.data) {
                showQuestion(message.data as IRMsgDataAppQuestion);
            }
            break;
        }
        case 'app_answer': {
            sendMsg({
                timeout: false,
                answer: ''
            }, 'app_answer');
            break;
        }
        case 'message':
            break;
        default:
            break;
    }
}

function insertApp(app: HTMLIFrameElement): HTMLIFrameElement {
    const appFrame: HTMLElement = protoGull.container.querySelector('#appFrame') as HTMLElement;
    return appFrame.appendChild(app);
}

function stopApp(code: string) {
    const app: IFrame | undefined = findApp(code);
    if (app !== undefined) {
        if (app.target) {
            const appFrame: HTMLElement = protoGull.container.querySelector('#appFrame') as HTMLElement;
            appFrame.removeChild(app.target);
        }
        app.target = undefined;
        app.state = 'waiting';
    }
}

function deleteApp(code: string) {
    stopApp(code);
    const app: IFrame | undefined = findApp(code);
    if (app !== undefined) {
        const index = protoGull.apps.indexOf(app);
        if (index !== -1) {
            protoGull.apps.splice(index, 1);
        }
    }
}

export function findApp(code: string): IFrame | undefined {
    return protoGull.apps.find(value => value.code === code);
}

export {protoGull};
