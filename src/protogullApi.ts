import {IMsg} from './interface/IMsg';
import {TFrameEvent} from './interface/TFrameEvent';

function getCustomerId() {
    sendToContainer({
        type: 'customerid',
        data: {},
        response: true
    })
}

function snapshot(data: {[param: string]: any}) {
    sendToContainer({
        type: 'snapshot',
        data,
        response: false
    })
}

function send(data: {[param: string]: any}, response: boolean = false) {
    sendToContainer({
        type: 'msg',
        data,
        response
    })
}

function trigger(eventType: TFrameEvent) {
    sendToContainer({
        type: 'event',
        data: {
            eventType
        },
        response: false
    })
}

function addEventListener(cb: (msg: IMsg) => void) {
    window.addEventListener('message', ev => {
        const {data} = ev;
        try {
            cb(data);
        } catch (e) {
            console.error(`error parsing`);
        }
    }, false);
}

function sendToContainer(msg: IMsg) {
    window.parent.postMessage(msg, '*')
}

document.addEventListener('DOMContentLoaded', () => trigger('loaded'));

export {getCustomerId, snapshot, send, trigger, addEventListener};
