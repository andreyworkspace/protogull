protogull
=============================

Прототип контейнера для отображения приложений


Files
------------
src/example/* - исходники примеров

src/protogull.ts - исходники контейнера

src/protogullApi.ts - исходники api для приложения встраиваемого в контейнер


Build
------------

``` js
npm run build
```

на выходе:

dist/api/protogullApi.js - api для приложения встраиваемого в контейнер

dist/container/protogull.js - код контейнера для подключения на сайтах

dist/apps - примеры приложений


API
------------
| Method | Arguments | Description |
| ------ | ------ | ----------- |
| getCustomerId | | запросить customerid |
| snapshot | data: JSONObject | отправить данны формы |
| send | data: JSONObject, response: boolean = false | отправить сообщение |
| trigger | eventType: TFrameEvent | отправить уведомление о событие |
| addEventListener | cb: (msg: IMsg) => void | подписаться на получение сообщений от контейнера |

``` js
type TFrameEvent = 'loaded' | 'start' | 'stop' | 'wait';

type TMsq = 'customerid' | 'msg' | 'event' | 'snapshot';

interface IMsg {
    type: TMsq;
    data: JSONObject;
    response: boolean;
}
```


Use container
------------
``` html
    <script data-url="ws://address" src="protogull.js"></script>
```

Use api
------------
Для использования api можно подключить\скопировать содержимое protogullApi.js
после этого будет доступен объект protogullApi.
Или же импортировать методы из исходников protogullApi.ts.

``` html
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>demo</title>
</head>
<body>
    <button disabled id="btn">click</button>
    <script>
    protogullApi.send({data: 1});
    protogullApi.trigger('loaded');
    protogullApi.addEventListener((msg) => {
        switch (msg.type) {
            case 'customerid':
                console.log('customerid', msg.data);
                break;
            case'snapshot':
                protogullApi.snapshot({test: 1});
                break;
            case 'msg':
                console.log(msg.type, msg.data);
                break;
        }
    })
    </script>
</body>
</html>

```

Utils
------------
Автоматическая загрузка демо приложений
``` js
npm run build && node uploadDemo.js
```
