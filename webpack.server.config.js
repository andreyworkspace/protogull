const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = [
    {
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    use: 'ts-loader',
                    exclude: /node_modules/
                },
                {
                    test: /\.css$/,
                    use: [
                        'style-loader',
                        'css-loader'
                    ]
                }
            ]
        },
        resolve: {
            extensions: ['.tsx', '.ts', '.js']
        },
        entry: {
            protogull: './src/protogull.ts'
        },
        output: {
            filename: '[name].js',
            path: path.resolve(__dirname, 'dist/container')
        },
        plugins: [
            new HtmlWebpackPlugin({
                inject: false,
                template: './src/index.ejs',
                filename: 'index.html'
            })
        ],
        devServer: {
            contentBase: path.join(__dirname, 'dist'),
            compress: true,
            port: 9000
        }
    }
];
