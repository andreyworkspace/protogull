var fs = require("fs");
var request = require('request');

[
    {
        "id": "a850efab-016b-954e-c18b-005056012700",
        "ext.code": "demo01"
    },
    {
        "id": "dd49b37e-016b-954f-212d-005056012700",
        "ext.code": "demo02"
    },
    {
        "id": "1a9ff497-016b-954f-8ca9-005056012700",
        "ext.code": "demo03"
    },
    {
        "id": "946abbe0-016b-954f-f825-005056012700",
        "ext.code": "demo04"
    }
].forEach(demo => {
    fs.readFile(`./dist/apps/${demo['ext.code']}.html`, 'utf-8', function (err, data) {
        if (err) {
            console.log("FATAL An error occurred trying to read in the file: " + err);
            process.exit(-2);
        }
        if(data) {
            var options = {
                uri: 'http://demo2.oktell.ru/api/admin/v1/extobj/update?td=rapps.demo2.oktell.ru&login=andrey&pwd=jdk38Hj',
                method: 'POST',
                json: {
                    "data": {
                        ...demo,
                        "type": "rapps",
                        "value": data,
                    },
                    "flat":"true"
                }};

            request(options, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    console.log(body)
                }
            });
        }
        else {
            console.log("No data to post");
            process.exit(-1);
        }
    });
});
