const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');

const conf = {
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            }
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    }
};

const containerConf = {
    ...conf,
    entry: {
        protogull: './src/protogull.ts'
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist/container')
    },
    plugins: [
        new HtmlWebpackPlugin({
            inject: false,
            template: './src/index.ejs',
            filename: 'index.html'
        })
    ],
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 9000
    }
};

const demoConf = {
    ...conf,
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist/apps')
    }
};

const apiConf = {
    ...conf,
    entry: {
        protogullApi: `./src/protogullApi.ts`
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist/api'),
        libraryTarget: 'var',
        library: '[name]'
    }
};

module.exports = [
    apiConf,
    containerConf,
    ...['01', '02', '03', '04'].map(demoNum => {
        return {
            ...demoConf,
            entry: {
                [demoNum]: `./src/example/demo${demoNum}/index.ts`
            },
            plugins: [
                new HtmlWebpackPlugin({
                    minify: {
                        collapseWhitespace: true,
                        removeComments: true,
                        removeRedundantAttributes: true,
                        removeScriptTypeAttributes: true,
                        removeStyleLinkTypeAttributes: true,
                        useShortDoctype: true
                    },
                    inlineSource: '.(js|css)$',
                    template: `./src/example/demo${demoNum}/index.html`,
                    filename: `demo${demoNum}.html`
                }),
                new HtmlWebpackInlineSourcePlugin()
            ]
        }
    })
];
